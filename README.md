### About
To show the issue with running a manual job with project token, which gives 403: Forbidden

The commands:
```
TOKEN="PROJECT_TOKEN_HERE"
project_id="PROJECT_ID_HERE"

# Create new pipeline
pipeline=$(curl -s --request POST --header "PRIVATE-TOKEN:${TOKEN}" "https://gitlab.com/api/v4/projects/${project_id}/pipeline?ref=main")
pipeline_id=$(echo ${pipeline} | jq '.id')

# Play the manual job
job_id=$(curl -s --request GET --header "PRIVATE-TOKEN:${TOKEN}" "https://gitlab.com/api/v4/projects/${project_id}/pipelines/${pipeline_id}/jobs" | jq ".[].id")
curl -s --request POST --header "PRIVATE-TOKEN:${TOKEN}" "https://gitlab.com/api/v4/projects/${project_id}/jobs/${job_id}/play"
```
